const {PORT, HOST} = require("./configuration")
const {connectDB} = require("./utils/db.js")
const {User} = require("./models/user.js")

const express = require("express")
const app = express()

app.get("/users", async (req, res)  => {
const user = new User({userName:"Yevhenii"});
await user.save();
const users = await User.find();
res.json({users});
});

function runServer() 
{
	app.listen(PORT, ()=> {
		console.log(`server working on ${HOST}:${PORT}`);	
});
}

app.get("/test", (req, res) =>{
	res.send('Server is working');
})

connectDB()
	.on("error", console.error.bind(console, "conection error "))
	.once("open", runServer);
